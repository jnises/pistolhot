﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpDownMover : MonoBehaviour
{
    public float fastness = 1;
    public float amplitude = 5;
    float? timeStart;
    Vector3 posStart;

    void Update()
    {
        if(!timeStart.HasValue) {
            timeStart = Time.time;
        }
        if(posStart == null) {
            posStart = transform.position;
        }
        transform.position = new Vector3(transform.position.x, posStart.y + Mathf.Sin((Time.time - timeStart.Value) * fastness) * amplitude, transform.position.z);
    }
}
