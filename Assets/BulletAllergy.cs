﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class BulletAllergy : MonoBehaviour
{
    Health health;

    void Update()
    {
        if(health == null) {
            health = GetComponent<Health>();
        }
    }

    void OnCollisionEnter(Collision collision) {
        if(collision.gameObject.tag == "Bullet") {
            Object.Destroy(collision.gameObject, 0);
            health.health -= 1;
        }
    }
}
