﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    public GameObject bullet;
    public float power = 20;
    public float lifetime = 0.5f;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space)) {
            var obj = Instantiate(bullet, transform.position + new Vector3(1, 0, 0), Quaternion.identity);
            Object.Destroy(obj, lifetime);
            var rb = obj.GetComponent<Rigidbody>();
            rb.velocity = new Vector3(power, 0, 0);
        }
    }
}
