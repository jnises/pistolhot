﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementLimiter : MonoBehaviour
{
    public float horizontal = 10;
    public float vertical = 10;
    public Transform anchor;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = new Vector3(Mathf.Clamp(transform.localPosition.x, - horizontal / 2, horizontal / 2),
                                Mathf.Clamp(transform.localPosition.y, - vertical / 2, vertical / 2),
                                transform.localPosition.z);
    }

    void OnDrawGizmosSelected() {
        Gizmos.DrawWireCube(anchor.position, new Vector3(horizontal, vertical, 0));
    }
}
