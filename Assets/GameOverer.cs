﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverer : MonoBehaviour
{
    public GameObject player;
    Canvas canvas;

    void Start() {
        canvas = GetComponent<Canvas>();
    }

    void Update()
    {
        canvas.enabled = player == null;
        if(canvas.enabled) {
            if(Input.GetKeyDown(KeyCode.Space)) {
                SceneManager.LoadScene("Main");
            }
        }
    }
}
