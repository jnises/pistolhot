﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour
{
    public float speed = 1;
    public GameObject player;

    void Update()
    {
        if (player)
        {
            transform.Translate(Vector3.right * Time.deltaTime * speed);
        }
    }
}
