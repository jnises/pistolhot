﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMover : MonoBehaviour
{
    public float playerSpeed = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var speed = Time.deltaTime * playerSpeed;
        if(Input.GetKey(KeyCode.UpArrow)) {
            transform.Translate(Vector2.up * speed); 
        }
        if(Input.GetKey(KeyCode.DownArrow)) {
            transform.Translate(Vector2.down * speed);
        }
        if(Input.GetKey(KeyCode.LeftArrow)) {
            transform.Translate(Vector2.left * speed);
        }
        if(Input.GetKey(KeyCode.RightArrow)) {
            transform.Translate(Vector2.right * speed);
        }
    }

    void OnGUI() {
        GUI.Label(new Rect(10, 10, 200, 30), "Arrow keys, and space.");
    }
}
