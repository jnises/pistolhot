﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public int health = 3;
    public GameObject deathSpawn;

    void Update()
    {
        if(health < 0) {
            Destroy(gameObject);
            if(deathSpawn != null) {
                var boom = Instantiate(deathSpawn, transform.position, Quaternion.identity);
                Destroy(boom, 5);
            }
        }
    }
}
